#ifndef KVASER_CAN_H
#define KVASER_CAN_H

#include "h/canlib.h"
#include "h/ControlCAN.h"

#define KVASER_DEVICE_TYPE VCI_USBCAN1

#define KVASER_MAX_CHANEL 4
#define KVASER_MAX_FILTERR 4

typedef enum _KVASER_DRIVER_STATE
{
	NoUsed = 0,
	Load,
	Init,
	Running,
	Uinit,
}KVASER_DRIVER_STATE;

typedef struct _KVASER_DEVICE
{
	CanHandle Handle;
	KVASER_DRIVER_STATE Step;
	ULONG ErrCount;
	ULONG RxCount;
	ULONG BusLoad;
	ULONG OverRuns;
	LARGE_INTEGER  Timer;
	VCI_CAN_STATUS Status;
	VCI_ERR_INFO ErrorInfo;
	UINT32 PASSIVE_Tick;
	VCI_INIT_CONFIG Config;
}KVASER_DEICE, * P_KVASER_DEVICE;

#endif